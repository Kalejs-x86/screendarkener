#include "../Common/Sources/WinAPI/Monitors.hpp"
#include "../Common/Sources/WinAPI/Window.hpp"
#include "resource.h"
#include <map>

class RAII_Darkener
{
public:
	RAII_Darkener()
	{
		auto Monitors = MonitorGetter.GetMonitors();
		
		for (auto Monitor : Monitors)
		{
			// Store original settings
			OldSettingsMap[Monitor.GetId()] = MonitorSettings(Monitor.GetBrightness(), Monitor.GetContrast());

			// Darken it for now
			Monitor.SetBrightness(0.0f);
			Monitor.SetContrast(0.0f);
		}
	}

	~RAII_Darkener()
	{
		// Restore old settings
		auto Monitors = MonitorGetter.GetMonitors();
		for (auto Monitor : Monitors)
		{
			MonitorSettings OldSettings;
			if (GetOldSettings(OldSettings, Monitor.GetId()))
			{
				Monitor.SetBrightness(OldSettings.Brightness);
				Monitor.SetContrast(OldSettings.Contrast);
			}
		}
	}

private:
	struct MonitorSettings
	{
		MonitorSettings() {};
		MonitorSettings(float Brightness, float Contrast)
		{
			this->Brightness = Brightness;
			this->Contrast = Contrast;
		}
		float Brightness = 0;
		float Contrast = 0;
	};

	std::map<Common::Monitor::Id_t, MonitorSettings> OldSettingsMap;
	Common::MonitorGetter MonitorGetter;

	bool GetOldSettings(MonitorSettings& Settings, Common::Monitor::Id_t Id)
	{
		if (OldSettingsMap.find(Id) != OldSettingsMap.end())
		{
			Settings = OldSettingsMap.at(Id);
			return true;
		}
		return false;
	}
};

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static std::unique_ptr<RAII_Darkener> Darkener = std::make_unique<RAII_Darkener>();

	switch (uMsg)
	{
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hwnd, &ps);
			HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
			FillRect(hdc, &ps.rcPaint, brush);
			EndPaint(hwnd, &ps);
			break;
		}
		case WM_CLOSE:
		{
			Darkener.reset();
			DestroyWindow(hwnd);
			return NULL;
		}
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return NULL;
		}
	}

	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

INT WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nCmdShow)
{
	Common::WindowCreateParams Params;
	Params.Instance = hInstance;
	Params.CmdShow = nCmdShow;
	Params.Width = Params.Height = 300;
	Params.SetResizable(false);
	Params.Icon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));

	Params.SetName("Screen Darkener");
	Common::Window window(Params);
	window.SetWindowProcedure(WindowProcedure);
	window.Create();


	Common::RunMessageLoop();
	return 0;
}